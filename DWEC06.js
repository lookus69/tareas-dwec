
var colorActual='color1';
var pincelActivado=false;
let tabla = document.getElementById('paleta');
tabla.addEventListener('click',setColor);
crearTablero(document.getElementById('zonadibujo'));
firma();

//tanto el numero de celdas como su tamaño se pueden modificar en la llamada a la funcion.
//los valores por defecto son los especificados en la tarea
function crearTablero(contenedor, celdas = 30, tamanio = 10){
  for(let y = 0; y < celdas; y++){
    for(let x = 0; x < celdas; x++){  
      let celda = document.createElement('div');
      celda.className = 'color6';
      //el codigo queda mucho mas limpio asignandole un estilo en el css, pero
      //si no se pudiese tocar la hoja de estilo o se quisiera tener un control del estilo desde js
      //se podrian descomentar y modificar las siguientes lineas y agregar las que sean necesarias.
      //celda.style.display = 'inline-block';
      //celda.style.border = 'thin solid lightgrey';
      //celda.style.margin = '1px';
      celda.style.width = tamanio + 'px';
      celda.style.height = tamanio + 'px';
      contenedor.appendChild(celda);
    }//for x
    contenedor.appendChild(document.createElement('br'));
  }//for y
  contenedor.addEventListener('mouseover',dibujar);
  contenedor.addEventListener('click',pincel);
}//crearTablero()

function dibujar(event){
  //console.log('e:'+event.target);
  if(event.target == event.currentTarget)
    return;
  if(pincelActivado)
    event.target.className = colorActual;
}//dibujar()

function pincel(){
  pincelActivado = !pincelActivado;
  let pincel = document.getElementById('pincel');
  pincel.innerHTML = pincelActivado ? 'PINCEL ACTIVADO' : 'PINCEL DESACTIVADO';
}//pincel()

function setColor(event){
  //aqui doy por hecho de que solo va a haber un elemento con clase 'seleccionado' por lo que lo selecciono directamente.
  //si hubiese mas elementos con esa clase, habria que buscar otra forma de seleccionarlo. lo mas facil hubiese sido poner 
  //id's a los elementos usados para seleccionar el color.
  let seleccionado = document.getElementsByClassName('seleccionado')[0];
  seleccionado.classList.remove('seleccionado');
  colorActual = event.target.classList[0];
  event.target.classList.add('seleccionado');
}//setColor()

function firma(){
  let lookus=document.createElement('div'); 
  lookus.appendChild(document.createTextNode('lucas Mónaco Fernández'));
  lookus.style.textAlign = 'right';
  lookus.style.position = 'fixed';
  lookus.style.right = '15px';
  lookus.style.bottom = '20px';
  document.body.appendChild(lookus);
  console.log('lucas Mónaco Fernández');
}//lookus