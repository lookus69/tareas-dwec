const bucleFor = document.getElementById("for");
const bucleWhile = document.getElementById("while");
const bucleDo = document.getElementById("do");
const bitshift = document.getElementById("bitshift");
bucleFor.innerHTML = "<h3>tabla de multiplicar</h3>bucle For" + tablaMultiplicar(7);
bucleWhile.innerHTML = "<h3>bucle while</h3>bucle while" + tablaSumar(8);
bucleDo.innerHTML = "<h3>bucle Do/While</h3>bucle do/while" + tablaDividir(9);
bitshift.innerHTML = "125 / 8 = " + dividir(125, 8) + "<br>";
bitshift.innerHTML += "40 x 4 = " + multiplicar(40, 4)+"<br>";
bitshift.innerHTML += "25 / 2 = " + dividir(25, 2)+"<br>";
bitshift.innerHTML += "10 x 16 = " + multiplicar(10, 16)+"<br>";

function tablaMultiplicar(numero){
    let resultado = "<ul>";
    for(let i=0; i <= 10; i++){
        resultado += "<li>" + numero + " x " + i + " = " + (numero * i) + "</li>";
    }
    resultado += "</ul>";
    return resultado;
}//tablaMultiplicar()

function tablaSumar(numero){
    let resultado = "<ul>";
    let i = 0;
    while (i <= 10){
        resultado += "<li>" + numero + " + " + i + " = " + (numero + i) + "</li>";
        i++;
    }
    resultado += "</ul>";
    return resultado;
}//tablaSumar()

function tablaDividir(numero){
    let resultado = "<ul>";
    let i = 0;
    do {
        resultado += "<li>" + numero + " / " + i + " = " + (numero / i) + "</li>";
        i++;
    }while (i <= 10);
    resultado += "</ul>";
    return resultado;
}//tablaDividir()

//esta funcion realiza la division desplazando bits
function dividir(dividendo, divisor){
    while (divisor >= 2){
        dividendo >>= 1;
        divisor >>=1 ;
    }//while
    return dividendo; //cociente
}//dividir()

//multiplica desplazando bits
function multiplicar(multiplicando, multiplicador){
    while (multiplicador >= 2){
        multiplicando <<= 1;
        multiplicador >>= 1;
        //console.log("x: " + multiplicando + " y: " + multiplicador);
    }
    return multiplicando; //producto
}//multiplicar()