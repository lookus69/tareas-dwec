

let formulario = document.getElementById('formulario');
let nombre = document.getElementById('nombre');
let apellidos = document.getElementById('apellidos');

formulario.addEventListener('submit',function(event){
  event.preventDefault();
  if(validarFormulario() && confirm('Desea enviar el formulario?')){
    actualizarIntentos(true);
    this.submit();
  }
  actualizarIntentos();
});

nombre.addEventListener('blur',mayus);
apellidos.addEventListener('blur',mayus);
firma();

function validarFormulario(){
  let errores = document.getElementById('errores');
  errores.innerHTML = ''; //se eliminan todos los mensajes de error anteriores
  let valido = true;
  if(!validarNombre()){
    valido = false;
    errores.innerHTML += '-los campos Nombre y Apellidos no pueden estar vacios!<br>';
  }
  if(!validarEdad()){
    valido = false;
    errores.innerHTML += '-el campo Edad solo puede contener valores numericos entre 0 y 105 (la tarea no permite que usted sea mas viejo)<br>';
  }
  if(!validarNIF()){
    valido = false;
    errores.innerHTML += '-el NIF debe contener 8 numeros y 1 letra<br>';
  }
  if(!validarEmail()){
    valido = false;
    errores.innerHTML += '-el email no tiene un formato valido<br>';
  }
  if(!validarProvincia()){
    valido = false;
    errores.innerHTML += '-debe seleccionar la provincia<br>';
  }
  if(!validarFecha()){
    valido = false;
    errores.innerHTML += '-la fecha debe estar en el formato dd-mm-aaaa<br>';
  }
  if(!validarTelefono()){
    valido = false;
    errores.innerHTML += '-el telefono debe contener 9 digitos<br>';
  }
  if(!validarHora()){
    valido = false;
    errores.innerHTML += '-la hora debe tener el formato hh:mm<br>';
  }


  return valido;
} //validarFormulario()

function actualizarIntentos(reset=false){
  let intentosDiv = document.getElementById('intentos');
  let intentos = Number(getCookie('intentos'));
  intentos += 1;
  if(reset)
    intentos = 0;
  intentosDiv.innerHTML = 'Intentos de envio del formulario: ' + getCookie('intentos');
  setCookie('intentos',intentos);
}

function setCookie(cname, cvalue){
  document.cookie = cname + '=' + cvalue;
} //setCookie()

function getCookie(cname){
  cname += '=';
  let cookie = document.cookie;
  let cArray = cookie.split('; ');
  for (let i = 0; i < cArray.length; i++){
    let c = cArray[i];
    if(c.indexOf(cname) == 0){
      return c.substring(cname.length);
    }
  }
  return false;
}//getCookie()

function mayus(evento){
  evento.currentTarget.value = evento.currentTarget.value.toUpperCase();
}
function validarNombre(){
  //la tarea no especifica como validar los campos 'nombre' y 'apellidos' por lo que unicamente se comprobará que no esten vacios.
  let n = document.getElementById('nombre');
  let a = document.getElementById('apellidos');
  let valido = n.value.length > 0;
  if(!valido){
    n.select();
    return false;
  }
  valido = a.value.length > 0;
  if(!valido){
    a.select();
    return false;
  }
  return true;
} //validarNombre()

function validarEdad(){
  let edadInput = document.getElementById('edad');
  let edad = edadInput.value;
  let valido = edad.length > 0 && Number(edad) >= 0 && Number(edad) <= 105;
  if(!valido){
    //edadInput.focus();
    edadInput.select();
  }
  return valido;
} //validarEdad()

function validarNIF(){
  let nif = document.getElementById('nif');
  //el patron comprueba si hay exactamente 8 numeros uno o ningun guion y 1 letra, nada antes y nada despues
  if(/^[0-9]{8}-?[a-z]$/i.test(nif.value)){
    return true;
  }
  nif.select();
  return false;
} //validarNIF()

function validarEmail(){
  let email = document.getElementById('email');
  //no he encontrado la forma de evitar que se validen direcciones con 2 puntos seguidos sin ningun caracter entre ellos
  //a la vez que si se permite que haya 2 o mas puntos que no esten juntos
  if(/[\w\.]+@[\w\.]*\w+\.[a-z]+/i.test(email.value)){
    return true;
  }
  email.select()
  return false;
} //validarEmail()

function validarProvincia(){
  let provincia = document.getElementById('provincia');
  let indice = provincia.selectedIndex;
  if(indice != 0){
    return true;
  }
  provincia.focus();
  return false;
} //validarProvincia()

function validarFecha(){
  let fecha = document.getElementById('fecha');
  if(/^\d{2}[\/\-]\d{2}[\/\-]\d{4}$/.test(fecha.value)){
    return true;
  }
  fecha.focus();
  return false;
} //validarFecha()

function validarTelefono() {
  let telefono = document.getElementById('telefono');
  if(/^\d{9}$/.test(telefono.value)){
    return true;
  }
    telefono.focus();
    return false;
} //validarTelefono()

function validarHora(){
  let hora = document.getElementById('hora');
  if(/^\d{2}:\d{2}$/.test(hora.value)){
    return true;
  }
  hora.focus();
  return false;
} //validarHora()

function firma(){
  let lookus=document.createElement('div'); 
  lookus.appendChild(document.createTextNode('lucas Mónaco Fernández'));
  lookus.style.textAlign = 'right';
  lookus.style.position = 'fixed';
  lookus.style.right = '15px';
  lookus.style.bottom = '20px';
  document.body.appendChild(lookus);
}
//lookus