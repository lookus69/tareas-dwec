nuevaVentana();
document.write("<h1>TAREA DWEC03</h1><hr/>"); //en la tarea pide cerrar una etiqueta h2, supongo que sera una errata
let nombre = prompt("introduzca su nombre y apellidos");
if(!nombre){    //si no se introduce un nombre, el programa no seguira, asique se asigna al menos una cadena vacia
    nombre = "";
}
let dia, mes, anio;
//una vez obtenido el nombre se comprueba si existe en las cookies y se recuperan los datos guardados
//si no hay una cookie con dicho nombre, se piden los datos y se crean las nuevas cookies para no tener que introducir todos los datos la proxima vez
if((nombre == getCookie("nombre")) && nombre){
    dia = getCookie("dia");
    mes = getCookie("mes");
    anio = getCookie("anio");
    alert("Bienvenido denuevo, "+nombre);
}else{
    dia = prompt("introduzca DIA de nacimiento");
    mes = prompt("introduzca MES de nacimiento");
    anio = prompt("introduzca AÑO de nacimiento");
    setCookie("nombre",nombre);
    setCookie("dia",dia);
    setCookie("mes",mes);
    setCookie("anio",anio);
}

//-> cadenas
document.write("Buenos dias "+nombre+"<br>");
document.write("Tu nombre tiene "+nombre.length+" caracteres, incluidos espacios.<br>");
const posicion = nombre.toLowerCase().indexOf("a");
if(posicion == -1){
    document.write("Tu nombre NO tiene A's<br>");
}else{
    document.write("La primera letra A de tu nombre esta en la posicion: "+(posicion+1)+"<br>");
    document.write("La Ultima letra A de tu nombre esta en la posicion: "+(nombre.toLowerCase().lastIndexOf("a")+1)+"<br>");
}
if(nombre.length<=3){
    document.write("Tu nombre tiene 3 letras o menos<br>");
}else{
    document.write("Tu nombre menos las 3 primeras letras es: "+nombre.substring(3)+"<br>");
}
document.write("Tu nombre todo en mayusculas es: "+nombre.toUpperCase()+"<br>");

//-> fechas
//al igual que con los datos anteriores, no se comprueba si se han introducido datos y ademas son numericos y dentro de los rangos validos para la fecha, ya que el ejercicio no lo pide
const fecha = new Date(anio,mes-1,dia);
const hoy = new Date();
const edad = (hoy.getTime() - fecha.getTime())/31556926216; // se pasan los milisegundos a años
//segun wikipedia un año tiene 31556926216ms ("The mean tropical year is 365.24219 days long.")
document.write("Tu edad es: "+Math.floor(edad)+" años<br>");
const dias = ["domingo","lunes","martes","miercoles","jueves","viernes","sabado"];
document.write("Naciste un feliz "+dias[fecha.getDay()] + " del año "+fecha.getFullYear()+"<br>");

//-> math
document.write("El coseno de 180 es: "+Math.cos(180 * Math.PI/180)+"<br>");
const numeros = [34,67,23,75,35,19];
document.write("El numero mayor de ("+numeros+") es: "+Math.max(...numeros)+"<br>");
document.write("Ejemplo de numero al azar: "+Math.random());

function nuevaVentana() {
    const ventana = window.open();
    ventana.document.write(firma());
    //ventana.document.open();
    ventana.document.title = "Ejemplo de Ventana Nueva";
    ventana.document.write("<h3>Ejemplo de Ventana Nueva</h3>");
    ventana.document.write("<p>URL Completa: "+ventana.location.href+"</p>");
    ventana.document.write("<p>Protocolo: "+ventana.location.protocol+"</p>");
    ventana.document.write("<p>Nombre en codigo del navegador: <abbr title=\"Esta propiedad se eliminó (obsoleta) en el estándar web más reciente.\nLa mayoria de buscadores devuelven 'Mozilla'\">"+ventana.navigator.appCodeName+"</abbr></p>")
    if(ventana.navigator.javaEnabled()){
        ventana.document.write("<p>Java SI disponible en esta ventana</p>");
    }else{
        ventana.document.write("<p><abbr title=\"este metodo tambien ha quedado obsoleto\">Java</abbr> NO disponible en esta ventana</p>");
    }
    //la web de google no carga en un iframe, he encontrado este "apaño" en stackoverflow, pero no tengo idea de que significa! :D
    ventana.document.write("<iframe src=\"http://www.google.es/webhp?igu=1\" width=\"800\" height=\"600\"/>");
    
    //ventana.document.close();
}
function setCookie(cNombre,cValor){
    document.cookie = cNombre + "=" + cValor;
}

function getCookie(cNombre){
    //se busca el inicio del nombre de la cookie
    let inicio = document.cookie.indexOf(cNombre+"=");
    if (inicio==-1) return false;
    //si el nombre de la cookie existe, el indice del primer caracter del valor de ésta, sera justo al final del nombre mas el "=":
    inicio += cNombre.length + 1;
    //ahora hay que calcular el final, que puede ser el ; que separa la siguiente cookie o el final de la cadena si es la ultima cookie
    let fin = document.cookie.indexOf(";",inicio);
    //si no se encuentra el ; se devuelve la cadena hasta el final, y sino se devuelve hasta el ; (no incluido)
    if(fin == -1) return document.cookie.substring(inicio);
    return document.cookie.substring(inicio,fin);
}

function firma(){
    return "<footer style=\"position:fixed;right:5%;bottom:5%;text-align:right;\">lucas Mónaco Fernández</footer>";
}