
window.addEventListener('load',iniciarPeticiones);
firma();
//todas las apis que encontré devuelven datos en json, asique he creado un script php que 
//devuelve datos en xml para poder hacer la tarea, espero que sea suficiente.
function iniciarPeticiones(){
  peticionAjax();
  peticionAjax('xml');
  $.get('http://lookus69.elargentino.es/api/pizzaXML.php',null,jqXmlToHtml);
  $.getJSON('https://epic.gsfc.nasa.gov/api/natural',null,jqJsonToHtml);
}
function peticionAjax(modo='json',tipo='ajax'){
  let ajax = new XMLHttpRequest();
  let direccion = modo == 'json' ? 'https://epic.gsfc.nasa.gov/api/natural' : 'http://lookus69.elargentino.es/api/pizzaXML.php';
  ajax.onreadystatechange = function(){
    if(this.readyState == 4 && this.status == 200){
      //console.log('modo:' + modo);
      if(modo == 'json'){
        //console.log('json ajax');
       // console.log('response: ' + this.getAllResponseHeaders());
        mostrarJSON(this,tipo);
      }else{
       // console.log('xml ajax');
        //console.log('response: ' + this.getAllResponseHeaders());
        mostrarXML(this,tipo);
      }
    }
  }//onreadystatechange
  //console.log('direccion: ' + direccion);
  ajax.open('GET',direccion);
  ajax.send();
}//peticionAjax

//puesto que no he sido capaz de agregar un parametro a la funcion en la respuesta jQuery para indicar el tipo de peticion realizada
//he creado estas funciones a modo de interfaz con las funciones que se encargan de extraer los datos y mostrarlos en la pagina.
function jqJsonToHtml(datos,estado,hxr){
  mostrarJSON(hxr,'jq');
}//jqToHtml()
function jqXmlToHtml(datos,estado,hxr){
  mostrarXML(hxr,'jq');
}

function mostrarXML(httpR,contenedor){
  let div = document.getElementById('xml' + contenedor);
  let xml = '<h4>Pizza del dia</h4>';
  let pizza = httpR.responseXML.getElementsByTagName("pizza")[0];
  xml += '<a href="' + pizza.getElementsByTagName("imagen")[0].innerHTML + '" target="_blank">';
  xml += '<img src="' + pizza.getElementsByTagName("miniatura")[0].innerHTML + '" title="pizza del dia"></a>';
  xml += '<p>' + pizza.getElementsByTagName("ingredientes")[0].innerHTML + '</p>';
  div.innerHTML = xml;
}

function mostrarJSON(httpR,contenedor){
  let div = document.getElementById('json' + contenedor);
  divstring = '';
  let pic = JSON.parse(httpR.responseText);
  for(let i  = 0; i < pic.length; i++){
    divstring += '<div class="thumbs">';
    divstring += 'fecha: ' + pic[i].date + '<br>';
    divstring += getMarbleLink(pic[i].date, pic[i].image,pic[i].caption);
    divstring += '</div>';
    div.innerHTML = divstring;
    //div.innerHTML += pic[i].caption + '<br>';
  }
}//mostrarJSON

function getMarbleLink(fecha, imagen, titulo = ''){
  let baseUrl = 'https://epic.gsfc.nasa.gov/archive/natural/';
  let anio = fecha.substr(0,4);
  let mes = fecha.substr(5,2);
  let dia = fecha.substr(8,2);
  let url = baseUrl + anio + '/' + mes + '/' + dia + '/jpg/' + imagen + '.jpg';
  let thumb = baseUrl + anio + '/' + mes + '/' + dia + '/thumbs/' + imagen + '.jpg';
  let enlace = '<a href="'+url+'" target="blank"><img src="'+thumb+'" title="'+titulo+'"></a>';
  //console.log('enlace:'+enlace);
  return enlace;
  
}//getMarbleLink

function firma(){
  let lookus=document.createElement('div'); 
  lookus.appendChild(document.createTextNode('lucas Mónaco Fernández'));
  lookus.style.textAlign = 'right';
  lookus.style.position = 'fixed';
  lookus.style.right = '15px';
  lookus.style.bottom = '20px';
  document.body.appendChild(lookus);
  console.log('lucas Mónaco Fernández');
}//lookus