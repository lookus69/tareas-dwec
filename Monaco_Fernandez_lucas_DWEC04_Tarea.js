class Edificio{

    constructor(calle, numero, cp){
        this.calle = calle;
        this.numero = numero;
        this.cp = cp;
        this.pisos = [];
        alert("construido nuevo edificio en la calle: " + this.calle + ", nº: " + this.numero + ", CP: " + this.cp);
    }//constructor

    /*para agregar mas plantas y puertas, teniendo en cuenta que no tiene por que haber el mismo
    numero de puertas por planta, lo que hace es crear un array de puertas por cada planta e ir 
    agregandolo al array de pisos del objeto.
    se llenan las puertas con una cadena vacia porque el metodo forEach de los array no se ejecuta 
    en los elementos vacios y en el ejemplo de la tarea se puede ver como para los pisos vacios
    tambien imprime una linea de texto sin el nombre del propietario.
    */
    agregarPlantasYPuertas(plantas, puertas){
        for (let p = 0; p < plantas; p++){
            let aPuertas = [];
            aPuertas.length = puertas;
            aPuertas.fill("");
            this.pisos.push(aPuertas);
        }
        //console.log(this.pisos);
    }//agregarPlantasYPuertas()

    modificarNumero(numero){
        this.numero = numero;
    }//modificarNumero()

    modificarCalle(calle){
        this.calle = calle;
    }//modificarCalle()

    modificarCodigoPostal(cp){
        this.cp = cp;
    }//modificarCodigoPostal()

    imprimeCalle(){
        return this.calle;
    }//imprimeCalle()

    imprimeNumero(){
        return this.numero;
    }//imprimeNumero()

    imprimeCodigoPostal(){
        return this.cp;
    }//imprimeCodigoPostal()

    /*se asigna el nombre del propietario a la posicion del array que corresponda
    teniendo en cuenta que los indices de los array empiezan en 0
    */
    agregarPropietario(nombre, planta, puerta){
        this.pisos[planta-1][puerta-1] = nombre;
        //alert(this.pisos[planta-1][puerta-1] + " es ahora el propietario de la puerta " + puerta + " de la planta " + planta);
        return this.pisos[planta-1][puerta-1] + " es ahora el propietario de la puerta " + puerta + " de la planta " + planta;
    }//agregarPropietario()

    /*recorre las plantas una a una, y por cada planta va agregando una cadena de texto con 
    los datos de cada piso al array de propietarios que será devuelto al finalizar
    */
    imprimePlantas(){
        let propietarios = [];
        this.pisos.forEach((planta, numPlanta) => {
            planta.forEach((piso, numPiso) =>{
                propietarios.push("Propietario del piso "+ (numPiso + 1) +" de la planta " + (numPlanta + 1) + ": " + piso);
            });
        });
        return propietarios;
    }//imprimePlantas()

    /*puesto que la tarea no especifica como o donde mostrar los datos solicitados, este metodo, utilizando el array devuelto por
    el metodo anterior, devuelve una sola cadena formateada con etiquetas html para mostrar dichos datos en la pagina
    con una sola llamada a este metodo
    */
    imprimePlantasHTML(){
        let propietarios = this.imprimePlantas();
        let propietariosHTML = "<div class= \"propietarios\">";
        propietariosHTML += "<h3>Listado de propietarios del edificio calle " + this.calle + " número " + this.numero + "</h3>";
        propietarios.forEach(propietario => {
            propietariosHTML += "<span>";
            propietariosHTML += propietario;
            propietariosHTML += "</span><br/>";
        });
        propietariosHTML += "</div>";
        return propietariosHTML;
    }//imprimePlantasHTML()
//lucas Mónaco Ferández
}//class Edificio
